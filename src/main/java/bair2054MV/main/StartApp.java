package bair2054MV.main;

import bair2054MV.model.Employee;
import bair2054MV.repository.implementations.EmployeeRepository;
import bair2054MV.repository.interfaces.EmployeeRepositoryInterface;
import bair2054MV.repository.mock.EmployeeMockRepository;
import bair2054MV.validator.EmployeeValidator;
import bair2054MV.controller.EmployeeController;
import bair2054MV.enumeration.DidacticFunction;
import bair2054MV.view.UserInterface;

//functionalitati
//F01.	 adaugarea unui nou angajat (nume, prenume, CNP, functia didactica, salariul de incadrare);
//F02.	 modificarea functiei didactice (asistent/lector/conferentiar/profesor) a unui angajat;
//F03.	 afisarea salariatilor ordonati descrescator dupa salariu si crescator dupa varsta (CNP).

public class StartApp {
	
	public static void main(String[] args) {

		EmployeeRepository employeeRepository=new EmployeeRepository();
		EmployeeController employeeController=new EmployeeController(employeeRepository);
		UserInterface userInterface=new UserInterface(employeeController);
		userInterface.show();
	}

}
