package bair2054MV.controller;

import java.util.List;

import bair2054MV.enumeration.DidacticFunction;
import bair2054MV.exception.EmployeeException;
import bair2054MV.model.Employee;
import bair2054MV.repository.interfaces.EmployeeRepositoryInterface;

public class EmployeeController {

	private EmployeeRepositoryInterface employeeRepository;

	public EmployeeController(EmployeeRepositoryInterface employeeRepository) {
		this.employeeRepository = employeeRepository;
	}

	public boolean addEmployee(Employee employee) throws EmployeeException{
		if(getEmployeeById(employee.getId())==null )
			return employeeRepository.addEmployee(employee);
		else
			throw new EmployeeException("ID already exists");
	}

	public List<Employee> getEmployeesList() {
		return employeeRepository.getEmployeeList();
	}

	public void modifyEmployee(int id, DidacticFunction function,double salary) {
		employeeRepository.modifyEmployee(id,function, salary);
	}

	public void deleteEmployee(Employee employee) {
		employeeRepository.deleteEmployee(employee);
	}

	public Employee getEmployeeById(int id){
		return employeeRepository.getEmployeeById(id);
	}

	public void sortList() throws EmployeeException{
		employeeRepository.sortList();
	}

}