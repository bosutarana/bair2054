package bair2054MV.validator;

import bair2054MV.enumeration.DidacticFunction;
import bair2054MV.model.Employee;

public class EmployeeValidator {
	
	public EmployeeValidator(){}

	public boolean isValid(Employee employee) {
		boolean isIdValid=employee.getId()>0;
		boolean isFirstNameValid  = employee.getFirstName().matches("[A-Z][a-z]+") && (employee.getFirstName().length() > 2);
		boolean isLastNameValid  = employee.getLastName().matches("[A-Z][a-z]+") && (employee.getLastName().length() > 2);
		boolean isCNPValid       = employee.getCnp().matches("[0-9]+") && (employee.getCnp().length() == 13);
		boolean isFunctionValid  = employee.getFunction().equals(DidacticFunction.ASISTENT) ||
				employee.getFunction().equals(DidacticFunction.LECTURER) ||
				employee.getFunction().equals(DidacticFunction.TEACHER)||
				employee.getFunction().equals(DidacticFunction.ASSOCIATE);
		boolean isSalaryValid    =employee.getSalary()>0;

		return isIdValid && isFirstNameValid && isLastNameValid && isCNPValid && isFunctionValid && isSalaryValid;
				}

	
}
