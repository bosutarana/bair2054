package bair2054MV.model;

import bair2054MV.enumeration.DidacticFunction;
import bair2054MV.exception.EmployeeException;
import bair2054MV.validator.EmployeeValidator;

public class Employee {
    /* Unique id*/
    private int id;
    /**
     * first name of the employee
     */
    private String firstName;
    /**
     * The last name of the employee
     */
    private String lastName;
    /**
     * The unique id of the employee
     */
    private String cnp;
    /**
     * The didactic function of the employee inside the university
     */
    private DidacticFunction function;

    /**
     * The salary of the employee
     */
    private double salary;

    /**
     * Default constructor for employee
     */
    public Employee() {
        this.id = 0;
        this.lastName = "";
        this.firstName="";
        this.cnp = "";
        this.function = DidacticFunction.ASISTENT;
        this.salary = 0.0;
    }

    /**
     * Constructor with fields for employee
     */
    public Employee(int Id,String firstName, String lastName, String cnp, DidacticFunction function, double salary) {
        this.id = Id;
        this.lastName = lastName;
        this.firstName=firstName;
        this.cnp = cnp;
        this.function = function;
        this.salary = salary;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Getter for the employee last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Setter for the employee last name
     *
     * @param lastName the last name to be set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Getter for the employee CNP
     */
    public String getCnp() {
        return cnp;
    }

    /**
     * Setter for the employee CNP
     *
     * @param cnp the CNP to be set
     */
    public void setCnp(String cnp) {
        this.cnp = cnp;
    }

    /**
     * Getter for the employee didactic function
     */
    public DidacticFunction getFunction() {
        return function;
    }

    /**
     * Setter for the employee function
     *
     * @param function the function to be set
     */
    public void setFunction(DidacticFunction function) {
        this.function = function;
    }

    /**
     * Getter for the employee salary
     */
    public double getSalary() {
        return salary;
    }

    /**
     * Setter for the salary
     *
     * @param salary the salary to be set
     */
    public void setSalary(double salary) {
        this.salary = salary;
    }

    /**
     * toString function for employee
     */
    @Override
    public String toString() {
        String employee = "";
        employee += id + ";";
        employee += firstName + ";";
        employee += lastName + ";";
        employee += cnp + ";";
        employee += function.toString() + ";";
        employee += salary;

        return employee;
    }

    /**
     * equals function for employee
     */
    public boolean equals(Employee comparableEmployee) {
        boolean hasSameId=this.id==comparableEmployee.getId(),
                hasSameFirstName  = this.firstName.equals(comparableEmployee.getFirstName()),
                hasSameLastName  = this.lastName.equals(comparableEmployee.getLastName()),
                hasSameCNP       = this.cnp.equals(comparableEmployee.getCnp()),
                hasSameFunction  = this.function.equals(comparableEmployee.getFunction()),
                hasSameSalary    = this.salary==comparableEmployee.getSalary();
        return hasSameId && hasSameFirstName && hasSameLastName && hasSameCNP && hasSameFunction && hasSameSalary;
    }

    /**
     * Returns the Employee after parsing the given line
     *
     * @param _employee the employee given as String from the input file
     * @param line      the current line in the file
     * @return if the given line is valid returns the corresponding Employee
     * @throws EmployeeException
     */
    public static Employee getEmployeeFromString(String _employee, int line) throws EmployeeException {
        Employee employee = new Employee();

        String[] attributes = _employee.split("[;]");

        if (attributes.length != 6) {
            throw new EmployeeException("Invalid line at: " + line);
        } else {
            EmployeeValidator validator = new EmployeeValidator();
            employee.setId(Integer.parseInt(attributes[0]));
            employee.setFirstName(attributes[1]);
            employee.setLastName(attributes[2]);
            employee.setCnp(attributes[3]);

            if (attributes[4].equals("ASISTENT"))
                employee.setFunction(DidacticFunction.ASISTENT);
            else if (attributes[4].equals("LECTURER"))
                employee.setFunction(DidacticFunction.LECTURER);
            else if (attributes[4].equals("TEACHER"))
                employee.setFunction(DidacticFunction.TEACHER);
            else if(attributes[4].equals("ASSOCIATE"))
                employee.setFunction(DidacticFunction.ASSOCIATE);
            else
                throw  new EmployeeException("Wrong Title at: "+ line);

            employee.setSalary(Double.parseDouble(attributes[5]));

            if (!validator.isValid(employee)) {
                throw new EmployeeException("Invalid line at: " + line);
            }
        }

        return employee;
    }

}
