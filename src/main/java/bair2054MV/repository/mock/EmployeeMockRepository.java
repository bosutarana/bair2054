package bair2054MV.repository.mock;

import java.util.ArrayList;
import java.util.List;

import bair2054MV.enumeration.DidacticFunction;

import bair2054MV.exception.EmployeeException;
import bair2054MV.model.Employee;
import bair2054MV.repository.implementations.EmployeeRepository;
import bair2054MV.repository.interfaces.EmployeeRepositoryInterface;
import bair2054MV.validator.EmployeeValidator;

public class EmployeeMockRepository implements EmployeeRepositoryInterface {

	private List<Employee> employeeList;
	private EmployeeValidator employeeValidator;
	
	public EmployeeMockRepository() {
		
		employeeValidator = new EmployeeValidator();
		employeeList = new ArrayList<Employee>();
		
		Employee Ionel   = new Employee(1,"Ana","Pacuraru", "1234567890876", DidacticFunction.ASISTENT, 2500);
		Employee Mihai   = new Employee(2,"Ioana","Dumitrescu", "1234567890876", DidacticFunction.LECTURER, 2500);
		Employee Ionela  = new Employee(3,"Ana","Ionescu", "1234567890876", DidacticFunction.LECTURER, 2500);
		Employee Mihaela = new Employee(4,"Ana","Pacuraru", "1234567890876", DidacticFunction.ASISTENT, 2500);
		Employee Vasile  = new Employee(5,"Ana","Georgescu", "1234567890876", DidacticFunction.TEACHER,  2500);
		Employee Marin   = new Employee(6,"Ana","Puscas", "1234567890876", DidacticFunction.TEACHER,  2500);
		
		employeeList.add( Ionel );
		employeeList.add( Mihai );
		employeeList.add( Ionela );
		employeeList.add( Mihaela );
		employeeList.add( Vasile );
		employeeList.add( Marin );
	}
	
	@Override
	public boolean addEmployee(Employee employee) {
		if ( employeeValidator.isValid(employee)) {
			employeeList.add(employee);
			return true;
		}
		return false;
	}
	
	@Override
	public void deleteEmployee(Employee employee) {
		employeeList.remove(employee);
	}

	@Override
	public void modifyEmployee(int id, DidacticFunction function, double salary) {
		Employee employee = getEmployeeById(id);
		if (employee != null) {
			employee.setFunction(function);
			employee.setSalary(salary);
		} else
			throw new RuntimeException("Nu exista employee cu id: " + id);



	}

	@Override
	public List<Employee> getEmployeeList() {
		return employeeList;
	}

	@Override
	public Employee getEmployeeById(int id) {

		for (Employee employee : employeeList) {
			if (employee.getId() == id)
				return employee;
		}
		return null;
	}

	@Override
	public void getDataFromFile() {
		//Not necessary for mock repository
	}

	@Override
	public void sortList() throws EmployeeException {
		if (employeeList.size() == 0)
			throw new EmployeeException("Can't sort list because is empty");
		employeeList.sort(EmployeeRepository::COMPARATOR);

	}

	/**
	 * @throws EmployeeException
	 */
	public void addRaise() throws EmployeeException {
		int sum = 0, index = 0;
		if (employeeList.size() == 0)
			throw new EmployeeException("nu exista angajati");

		while (sum < 10000 && index < employeeList.size()) {
			Employee employee = employeeList.get(index);
			if (employee.getFunction() == DidacticFunction.TEACHER) {
				employee.setSalary(employee.getSalary() + 500);
				sum += 500;
			} else if (employee.getFunction() == DidacticFunction.ASISTENT) {
				employee.setSalary(employee.getSalary() + 300);
				sum += 300;
			}
			index++;
		}
	}
}
