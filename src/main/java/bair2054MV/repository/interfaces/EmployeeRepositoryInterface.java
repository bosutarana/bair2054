package bair2054MV.repository.interfaces;

import java.util.List;

import bair2054MV.enumeration.DidacticFunction;
import bair2054MV.exception.EmployeeException;
import bair2054MV.model.Employee;

public interface EmployeeRepositoryInterface {

	boolean addEmployee(Employee employee);
	void deleteEmployee(Employee employee);
	void modifyEmployee(int id, DidacticFunction function,double salary);
	List<Employee> getEmployeeList();
	Employee getEmployeeById(int id);
	void getDataFromFile();
	void sortList() throws EmployeeException;


}
