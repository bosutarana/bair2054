package bair2054MV.repository.implementations;

import java.io.*;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import bair2054MV.enumeration.DidacticFunction;
import bair2054MV.exception.EmployeeException;

import bair2054MV.model.Employee;

import bair2054MV.repository.interfaces.EmployeeRepositoryInterface;
import bair2054MV.validator.EmployeeValidator;

public class EmployeeRepository implements EmployeeRepositoryInterface {

	private final String employeeDBFile = "D:\\An3\\Vvss\\bair2054\\employeeDB\\employees";
	private EmployeeValidator employeeValidator = new EmployeeValidator();
	private List<Employee> employeeList;

	public EmployeeRepository() {
		getDataFromFile();
	}

	@Override
	public boolean addEmployee(Employee employee) {
		if (employeeValidator.isValid(employee)) {
			BufferedWriter bw = null;
			try {

				bw = new BufferedWriter(new FileWriter(employeeDBFile, true));

				bw.write(employee.toString());
				bw.newLine();
				employeeList.add(employee);
				bw.close();
				return true;
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (bw != null)
					try {
						bw.close();
					} catch (IOException e) {
						System.err.println("Error while closing the file: " + e);
					}
			}
		}
		return false;
	}

	@Override
	public void deleteEmployee(Employee employee) {
		employeeList.remove(employee);
		writeToFile();

	}

	@Override
	public void modifyEmployee(int id, DidacticFunction function, double salary) {
		Employee employee = getEmployeeById(id);
		if (employee != null) {
			employee.setFunction(function);
			employee.setSalary(salary);
			writeToFile();
		} else
			throw new RuntimeException("No employee with id");


	}

	private void writeToFile() {
		BufferedWriter bw = null;
		try {

			bw = new BufferedWriter(new FileWriter(employeeDBFile));
			for (Employee employee : employeeList) {
				bw.write(employee.toString());
				bw.newLine();
			}

			bw.close();

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (bw != null)
				try {
					bw.close();
				} catch (IOException e) {
					System.err.println("Error while closing the file: " + e);
				}
		}

	}

	@Override
	public void getDataFromFile() {

		employeeList = new ArrayList<Employee>();
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(employeeDBFile));
			String line;
			int counter = 0;
			while ((line = br.readLine()) != null) {
				counter++;
				Employee employee = new Employee();
				try {
					if (line != "") {
						employee = (Employee) Employee.getEmployeeFromString(line, counter);
						employeeList.add(employee);
					}
				} catch (EmployeeException ex) {
					System.err.println("Error while reading: " + ex.toString());
				}
			}
		} catch (FileNotFoundException e) {
			System.err.println("Error while reading: " + e);
		} catch (IOException e) {
			System.err.println("Error while reading: " + e);
		} finally {
			if (br != null)
				try {
					br.close();
				} catch (IOException e) {
					System.err.println("Error while closing the file: " + e);
				}
		}


	}


	@Override
	public Employee getEmployeeById(int id) {
		for (Employee employee : employeeList) {
			if (employee.getId() == id)
				return employee;
		}
		return null;
	}

	@Override
	public List<Employee> getEmployeeList() {
		return employeeList;
	}

	@Override
	public void sortList() throws EmployeeException{
		if (employeeList.size() == 0)
			throw new EmployeeException("Can't sort list because is empty");
		//employeeList.sort(EmployeeRepository::COMPARATOR);
		employeeList.sort(EmployeeRepository::COMPARATOR);

	}

	/*
	size of list must be>0
	the method gives 300 bonus salary to Asistents and 500 to Teachers till
	the total sum is over 10000
	 */


	public static int COMPARATOR(Employee A, Employee B) {
		if (A.getSalary() > B.getSalary())
			return -1;
		else if (B.getSalary() > A.getSalary())
			return 1;
		String cnpA = A.getCnp();
		String cnpB = B.getCnp();
		int firstDigitA = Integer.parseInt(cnpA.substring(0, 1));
		int firstDigitB = Integer.parseInt(cnpB.substring(0, 1));

		if (firstDigitA - firstDigitB > 1)
			return 1;
		else if (firstDigitB - firstDigitA > 1)
			return -1;
		else {
			long numberA = Long.parseLong(cnpA.substring(1));
			long numberB = Long.parseLong(cnpB.substring(1));
			if (numberA > numberB)
				return 1;
			else if (numberA < numberB)
				return -1;
		}

		return 0;
	}

}
