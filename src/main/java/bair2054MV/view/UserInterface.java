package bair2054MV.view;

import bair2054MV.controller.EmployeeController;
import bair2054MV.enumeration.DidacticFunction;
import bair2054MV.exception.EmployeeException;
import bair2054MV.model.Employee;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.InputMismatchException;


public class UserInterface {
    private EmployeeController employeeController;
    public BufferedReader bufferedReader;

    public UserInterface(EmployeeController employeeController) {
        this.employeeController = employeeController;
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));

    }

    private void menu() {
        System.out.println("1.Add employee\n" +
                "2.Modify function of employee\n" +
                "3.See Employees\n" +
                "4.Delete an employee\n" +
                "5.AddReise\n"+
                "0.exit\n");
    }

    public void show() {
        String cmd;

        menu();


        while (true) {
            try {
                cmd = bufferedReader.readLine();
                if (cmd.equals("0"))
                    break;
                else if (cmd.equals("1")) {
                    try {
                        System.out.println("Dati id:");
                        int id = Integer.parseInt(bufferedReader.readLine());

                        System.out.println("Enter firstname:");
                        String firstname = bufferedReader.readLine();
                        System.out.println("Enter lastname:");
                        String lastname = bufferedReader.readLine();
                        System.out.println("Enter CNP:");
                        String cnp = bufferedReader.readLine();
                        System.out.println("Enter function");
                        DidacticFunction function = DidacticFunction.valueOf(bufferedReader.readLine());
                        System.out.println("Enter salary:");
                        double salary = Double.parseDouble(bufferedReader.readLine());

                        Employee employee = new Employee(id, firstname, lastname, cnp, function, salary);
                        boolean added = employeeController.addEmployee(employee);
                        if (added)
                            System.out.println("Added");
                        else
                            System.out.println("Name should start with capital letter\nCNP should have 13 digits\nSalary should be >0");

                    } catch (NumberFormatException e) {
                        System.out.println("id-ul si salariul trebuie sa fie numere");
                    } catch (IllegalArgumentException e) {
                        System.out.println("Nu exista aceasta functie\n");
                    }  catch (EmployeeException e) {
                        System.out.println(e.getMessage());
                    }

                } else if (cmd.equals("2")) {
                    try {
                        System.out.println("dati id:");
                        int id = Integer.parseInt(bufferedReader.readLine());

                        System.out.println("Dati functie noua:");
                        String functie = bufferedReader.readLine();
                        DidacticFunction employeeFunction = DidacticFunction.valueOf(functie);
                        System.out.println("Dati salariul nou:");
                        double salariu = Double.parseDouble(bufferedReader.readLine());

                        employeeController.modifyEmployee(id, employeeFunction, salariu);
                        System.out.println("function modified to "+functie);

                    } catch (NumberFormatException e) {
                        System.out.println("id-ul si salariul trebuie sa fie numere");
                    } catch (IllegalArgumentException e) {
                        System.out.println("Nu exista functia");
                    } catch (RuntimeException e) {
                        System.out.println("Nu exista acest angajat cu acest id");
                    }

                } else if (cmd.equals("3")) {
                    try {
                        employeeController.sortList();
                        for (Employee employee : employeeController.getEmployeesList()) {
                            System.out.println(employee);
                        }
                    } catch (EmployeeException e) {
                        System.out.println(e.getMessage());
                    }
                } else if (cmd.equals("4")) {
                    try {
                        System.out.println("Dati id:");
                        int id = Integer.parseInt(bufferedReader.readLine());

                        if (employeeController.getEmployeeById(id) != null) {
                            employeeController.deleteEmployee(employeeController.getEmployeeById(id));
                            System.out.println("Stergerea a fost efectuata");
                        } else
                            System.out.println("Nu este angajat cu id-ul:" + id);
                    } catch (NumberFormatException e) {
                        System.out.println("id-ul trebuie sa fie numar");
                    }

                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
