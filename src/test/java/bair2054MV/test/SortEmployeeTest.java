package bair2054MV.test;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import bair2054MV.controller.EmployeeController;
import bair2054MV.enumeration.DidacticFunction;
import bair2054MV.exception.EmployeeException;
import bair2054MV.model.Employee;
import bair2054MV.repository.interfaces.EmployeeRepositoryInterface;
import bair2054MV.repository.mock.EmployeeMockRepository;
import bair2054MV.validator.EmployeeValidator;

public class SortEmployeeTest {

    private EmployeeRepositoryInterface employeeRepository;
    private EmployeeController controller;
    private EmployeeValidator employeeValidator;

    @Before
    public void setUp() {
        employeeRepository = new EmployeeMockRepository();
        controller         = new EmployeeController(employeeRepository);
        employeeValidator  = new EmployeeValidator();
    }


    @Test
    public void sortListTest(){
        try {
            controller.addEmployee(new Employee(1500, "Xander", "Bla", "1970101123123", DidacticFunction.TEACHER, 3000));
            employeeRepository.sortList();
            assertTrue(employeeRepository.getEmployeeList().get(0).getSalary()==3000);
        }
        catch(EmployeeException e){
            e.printStackTrace();
        }
    }

    @Test
    public void sortListTestInvalid(){
        for(int i=1;i<=6;i++){
            controller.deleteEmployee(controller.getEmployeeById(i));
        }
        try{
            controller.sortList();
        }
        catch(EmployeeException e){
            assertTrue(e.getMessage().equals("Can't sort list because is empty"));
        }
    }
}

