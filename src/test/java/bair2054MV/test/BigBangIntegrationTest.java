package bair2054MV.test;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.internal.configuration.injection.scanner.MockScanner;
import bair2054MV.controller.EmployeeController;
import bair2054MV.enumeration.DidacticFunction;
import bair2054MV.exception.EmployeeException;
import bair2054MV.model.Employee;
import bair2054MV.repository.interfaces.EmployeeRepositoryInterface;
import bair2054MV.repository.mock.EmployeeMockRepository;
import bair2054MV.validator.EmployeeValidator;
import bair2054MV.view.UserInterface;

import java.io.*;
import java.util.Scanner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BigBangIntegrationTest {

    private EmployeeRepositoryInterface employeeRepository;
    private EmployeeController controller;
    private EmployeeValidator employeeValidator;
    private UserInterface userInterface;

    @Before
    public void setUp() {
        employeeRepository = new EmployeeMockRepository();
        controller = new EmployeeController(employeeRepository);
        employeeValidator = new EmployeeValidator();
        userInterface = new UserInterface(controller);

    }

    @Test
    public void f01_test() {
        try {

            Employee newEmployee = new Employee(10, "Alex", "Validlastname", "1910509055057", DidacticFunction.ASISTENT, 2);
            boolean added = controller.addEmployee(newEmployee);
            assertTrue(added);


        } catch (EmployeeException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void f02_test() {
        controller.modifyEmployee(1, DidacticFunction.TEACHER, 2600);
        assertTrue(controller.getEmployeeById(1).getFunction() == DidacticFunction.TEACHER);
    }

    @Test
    public void f03_test() {
        try {
            controller.addEmployee(new Employee(1500, "Xander", "Bla", "1970101123123", DidacticFunction.TEACHER, 4000));
            employeeRepository.sortList();
            assertTrue(employeeRepository.getEmployeeList().get(0).getSalary() == 4000);
        } catch (EmployeeException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void integration_test() {

        try {


            userInterface.bufferedReader = Mockito.mock(BufferedReader.class);

            Mockito.when(userInterface.bufferedReader.readLine()).thenReturn("1", "8000", "John", "Doe", "1980101123123", "ASISTENT", "4000",
                    "2", "8000", "TEACHER", "5000", "3", "0");
            ByteArrayOutputStream bo = new ByteArrayOutputStream();
            System.setOut(new PrintStream(bo));
            bo.flush();
            userInterface.show();
            String allWrittenLines = new String(bo.toByteArray());

            assertTrue(allWrittenLines.contains("Added"));
            assertTrue(allWrittenLines.contains("function modified to TEACHER"));
            assertTrue(allWrittenLines.contains("8000;John;Doe;1980101123123;TEACHER;5000.0"));
            assertTrue(controller.getEmployeesList().get(0).getSalary() == 5000);


        } catch (IOException e) {
            e.printStackTrace();
        }


    }


}
