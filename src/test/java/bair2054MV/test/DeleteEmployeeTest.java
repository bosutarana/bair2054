package bair2054MV.test;

import static org.junit.Assert.*;

import bair2054MV.controller.EmployeeController;
import bair2054MV.model.Employee;

import org.junit.Before;
import org.junit.Test;
import bair2054MV.repository.interfaces.EmployeeRepositoryInterface;
import bair2054MV.repository.mock.EmployeeMockRepository;
import bair2054MV.validator.EmployeeValidator;

public class DeleteEmployeeTest {
    private EmployeeRepositoryInterface employeeRepository;
    private EmployeeController controller;
    private EmployeeValidator employeeValidator;

    @Before
    public void setUp() {
        employeeRepository = new EmployeeMockRepository();
        controller         = new EmployeeController(employeeRepository);
        employeeValidator  = new EmployeeValidator();
    }

    @Test
    public void testRepositoryMock() {
        assertFalse(controller.getEmployeesList().isEmpty());
        assertEquals(6, controller.getEmployeesList().size());
    }

    @Test
    public void testDeleteEmployee() {
        employeeRepository.getDataFromFile();
        Employee employee=controller.getEmployeeById(1);
        controller.deleteEmployee(employee);
        assertTrue(controller.getEmployeesList().size()==5);
        employee.setId(100);
        controller.deleteEmployee(employee);
        assertTrue(controller.getEmployeesList().size()==5);
    }
}
