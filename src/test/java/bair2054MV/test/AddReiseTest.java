package bair2054MV.test;

import static org.junit.Assert.*;

import bair2054MV.enumeration.DidacticFunction;
import org.junit.Before;
import org.junit.Test;
import bair2054MV.controller.EmployeeController;
import bair2054MV.exception.EmployeeException;
import bair2054MV.model.Employee;
import bair2054MV.repository.implementations.EmployeeRepository;
import bair2054MV.repository.interfaces.EmployeeRepositoryInterface;
import bair2054MV.repository.mock.EmployeeMockRepository;
import bair2054MV.validator.EmployeeValidator;

public class AddReiseTest {
    private EmployeeMockRepository employeeRepository;


    @Before
    public void setUp() {
        employeeRepository = new EmployeeMockRepository();
        for(int i=1;i<=6;i++){
            employeeRepository.deleteEmployee(employeeRepository.getEmployeeById(i));
        }

    }

    @Test
    public void tc1(){
        assertTrue(employeeRepository.getEmployeeList().size()==0);
        try{
            employeeRepository.addRaise();
        }
        catch(EmployeeException e){
            assertTrue(e.getMessage().equals("nu exista angajati"));
        }

    }

    @Test
    public void tc2(){
        employeeRepository.addEmployee(new Employee(1,"Ana","Bosutar","2971219336687", DidacticFunction.TEACHER,3000));
        double salary=employeeRepository.getEmployeeById(1).getSalary();
        try{
            employeeRepository.addRaise();
            assertTrue(employeeRepository.getEmployeeById(1).getSalary()==salary+500);
        }
        catch(EmployeeException e){
            e.printStackTrace();
        }
    }

    @Test public void tc3(){
        employeeRepository.addEmployee(new Employee(1,"Ana","Bosutar","2971219336687", DidacticFunction.LECTURER,3000));
        employeeRepository.addEmployee(new Employee(2,"Ana","Bosutar","2971219336687", DidacticFunction.ASISTENT,3000));
        double salaryLector=employeeRepository.getEmployeeById(1).getSalary();
        double salaryAsistent=employeeRepository.getEmployeeById(2).getSalary();
        try{
            employeeRepository.addRaise();
            assertTrue(employeeRepository.getEmployeeById(1).getSalary()==salaryLector);
            assertTrue(employeeRepository.getEmployeeById(2).getSalary()==salaryAsistent+300);
        }
        catch(EmployeeException e){
            e.printStackTrace();
        }
    }

    @Test
    public void tc4(){
        for(int i=1;i<=21;i++){
            employeeRepository.addEmployee(new Employee(i,"Ana","Bosutar","2971219336687", DidacticFunction.TEACHER,3000));
        }
        try {
            employeeRepository.addRaise();
            int counter=0;
            for(Employee employee:employeeRepository.getEmployeeList()){
                if(employee.getSalary()==3500)
                    counter++;
            }
            assertTrue(counter==20);
        }
        catch(EmployeeException e){
            e.printStackTrace();
        }
    }
}
