package bair2054MV.test;

import static org.junit.Assert.*;

import bair2054MV.controller.EmployeeController;
import bair2054MV.enumeration.DidacticFunction;

import org.junit.Before;
import org.junit.Test;
import bair2054MV.repository.interfaces.EmployeeRepositoryInterface;
import bair2054MV.repository.mock.EmployeeMockRepository;
import bair2054MV.validator.EmployeeValidator;

public class ModifyEmployeeTest {

    private EmployeeRepositoryInterface employeeRepository;
    private EmployeeController controller;
    private EmployeeValidator employeeValidator;

    @Before
    public void setUp() {
        employeeRepository = new EmployeeMockRepository();
        controller         = new EmployeeController(employeeRepository);
        employeeValidator  = new EmployeeValidator();
    }

    @Test
    public void testRepositoryMock() {
        assertFalse(controller.getEmployeesList().isEmpty());
        assertEquals(6, controller.getEmployeesList().size());
    }

    @Test
    public void testModifyNewEmployee() {
        controller.modifyEmployee(1,DidacticFunction.TEACHER,2600);
        assertTrue(controller.getEmployeeById(1).getFunction()==DidacticFunction.TEACHER);
    }

    @Test
    public void modifyInexistentEmploye(){
        try {
            controller.modifyEmployee(123, DidacticFunction.TEACHER, 3000);
        }
        catch(RuntimeException e){
            assertTrue(e.getMessage().equals("Nu exista employee cu id: 123"));
        }

    }
}

