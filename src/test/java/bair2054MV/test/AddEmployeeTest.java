package bair2054MV.test;

import static org.junit.Assert.*;

import  bair2054MV.exception.EmployeeException;
import bair2054MV.model.Employee;

import org.junit.Before;
import org.junit.Test;

import bair2054MV.repository.interfaces.EmployeeRepositoryInterface;
import bair2054MV.repository.mock.EmployeeMockRepository;
import bair2054MV.validator.EmployeeValidator;
import bair2054MV.controller.EmployeeController;
import bair2054MV.enumeration.DidacticFunction;

public class AddEmployeeTest {

	private EmployeeRepositoryInterface employeeRepository;
	private EmployeeController controller;
	private EmployeeValidator employeeValidator;
	
	@Before
	public void setUp() {
		employeeRepository = new EmployeeMockRepository();
		controller         = new EmployeeController(employeeRepository);
		employeeValidator  = new EmployeeValidator();
	}

	@Test
	public void tc1_ecp() {
		try {

			Employee newEmployee = new Employee(10, "Alex", "Validlastname", "1910509055057", DidacticFunction.ASISTENT,2);
			assertTrue(employeeValidator.isValid(newEmployee));
			boolean added=controller.addEmployee(newEmployee);
			assertTrue(added);


		} catch (EmployeeException e) {
			e.printStackTrace();
		}
	}
	@Test
	public void tc3_ecp(){
		try {
			Employee newEmployee = new Employee(100, "abc123", "Validlastname", "1910509055057", DidacticFunction.ASISTENT, 24);

			boolean added=controller.addEmployee(newEmployee);
			assertFalse(added);

		}
		catch(EmployeeException e){
			e.printStackTrace();
		}
	}

	@Test
	public void tc4_ecp(){
		try{
			Employee newEmployee = new Employee(12, "Bo", "Validlastname", "1910509055057", DidacticFunction.ASISTENT, 3000);
			boolean added=controller.addEmployee(newEmployee);
			assertFalse(added);
		}
		catch(EmployeeException e){
			e.printStackTrace();
		}
	}

	@Test
	public void tc6_ecp(){
		try{
			Employee newEmployee = new Employee(13, "Ana", "Validlastname", "1910509055057",DidacticFunction.ASISTENT, 0);
			boolean added=controller.addEmployee(newEmployee);
			assertFalse(added);
		}
		catch (EmployeeException e){

		}
	}

	@Test
	public void tc1_bva(){
		try{
			Employee newEmployee = new Employee(14, "Maria", "Validlastname", "1910509055057", DidacticFunction.ASISTENT, 1);
			boolean added=controller.addEmployee(newEmployee);
			assertTrue(added);
		}
		catch (EmployeeException e){

		}
	}

	@Test
	public void tc4_bva(){
		try{

			Employee newEmployee = new Employee(14, "Maria", "Validlastname", "1910509055057", DidacticFunction.ASISTENT, Double.MAX_VALUE);
			boolean added=controller.addEmployee(newEmployee);
			assertTrue(added);
		}
		catch (EmployeeException e){

		}
	}

	@Test
	public void tc5_bva(){
		try{
			Employee newEmployee = new Employee(14, "Maria", "Validlastname", "1910509055057", DidacticFunction.ASISTENT, Double.MAX_VALUE-1);
			boolean added=controller.addEmployee(newEmployee);
			assertTrue(added);
		}
		catch (EmployeeException e){

		}
	}


	@Test
	public void tc8_bva(){
		try{
			Employee newEmployee = new Employee(14, "Maria", "Validlastname", "1910509055057", DidacticFunction.ASISTENT, Double.MAX_VALUE+1);
			boolean added=controller.addEmployee(newEmployee);
			assertTrue(added);
		}
		catch (EmployeeException e){

		}
	}

}
